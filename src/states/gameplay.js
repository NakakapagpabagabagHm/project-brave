import Player from '../objects/player';
import { Platform, LargePlatform, GoldPlatform, ArrowPlatform } from '../objects/platform';
import Apple from '../objects/apple';

class Gameplay extends Phaser.State {
  create() {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    this.game.physics.arcade.gravity.y = 2000;

    this.game.add.sprite(0, 0, 'bg_gameplay');

    this.createObjects(1);
    this.createHUD();
  }

  createObjects(level) {
    let levelData = this.cache.getJSON(`level${level}`);

    // Platforms
    this.platforms = [];
    let goldIndex = 0;
    let arrowIndex = 0;
    for (let i = 0; i < levelData.platforms.length; i++) {
      let platformData = levelData.platforms[i];
      let platform;
      if (platformData.type === 'normal') {
        platform = new Platform(this.game, this.game.world.centerX + platformData.x, this.game.world.centerY + platformData.y, 'platform_normal');
        platform.body.setSize(79, 20, 0, 23);
      } else if (platformData.type === 'large') {
        platform = new LargePlatform(this.game, this.game.world.centerX + platformData.x, this.game.world.centerY + platformData.y);
        platform.body.setSize(179, 20, 0, 33);
      } else if (platformData.type === 'gold') {
        platform = new GoldPlatform(this.game, this.game.world.centerX + platformData.x, this.game.world.centerY + platformData.y);
        platform.index = goldIndex;
        goldIndex++;
      } else if (platformData.type === 'arrow') {
        platform = new ArrowPlatform(this.game, this.game.world.centerX + platformData.x, this.game.world.centerY + platformData.y);
        platform.index = arrowIndex;
        arrowIndex++;
      }
      platform.anchor.set(0.5, 0.5);
      platform.body.allowGravity = false;
      platform.body.immovable = true;
      this.platforms.push(platform);
    }

    // Apple
    if (levelData.apple !== void 0) {
      this.apple = new Apple(this.game, this.game.world.centerX + levelData.apple.x, this.game.world.centerY + levelData.apple.y);
      this.apple.anchor.set(0.5, 0.5);
      this.apple.setSize(0);
      this.game.physics.arcade.enable(this.apple, false);
      this.apple.body.allowGravity = false;
      this.apple.body.immovable = false;
      this.apple.body.enable = false;
    }

    // Object Pools
    this.apples = [];
    for (let i = 0; i < 4; i++) {
      let a = this.game.add.sprite(0, 0, 'apple');
      a.anchor.set(0.5, 0.5);
      a.visible = false;
      this.apples.push(a);
    }

    this.arrows = [];
    for (let i = 0; i < arrowIndex; i++) {
      let a = this.game.add.sprite(0, 0, 'arrow');
      a.anchor.set(0.5, 0.5);
      a.visible = false;
      this.game.physics.arcade.enable(a);
      a.body.allowGravity = false;
      this.arrows.push(a);
    }

    // Goat
    this.goat = this.game.add.sprite(this.game.world.centerX + levelData.goat.x, this.game.world.centerY + levelData.goat.y, 'goat');
    this.goat.anchor.set(0.5, 0.5);
    this.goat.scale.x = levelData.goat.direction;
    this.game.physics.arcade.enable(this.goat);
    this.goat.body.allowGravity = false;
    this.goat.body.immovable = true;
    this.goat.body.enable = false;

    // Player
    this.player = new Player(this.game, this.game.world.centerX + levelData.player.x, this.game.world.centerY + levelData.player.y, 'steven');
    this.player.anchor.set(0.5, 0.5);
    this.player.scale.x = levelData.player.direction;
    this.game.physics.arcade.enable(this.player);
    this.player.body.setSize(30, 84, 50, 20);
    this.player.animations.play('idle');
  }

  createHUD() {
    this.btnRight = this.game.add.button(this.game.world.centerX - 380, this.game.world.height - 100, 'btn_right', null, this);
    this.btnRight.events.onInputOver.add(() => this.player.runRight = true);
    this.btnRight.events.onInputOut.add(() => this.player.runRight = false);
    this.btnRight.events.onInputDown.add(() => this.player.runRight = true);
    this.btnRight.events.onInputUp.add(() => this.player.runRight = false);
    this.btnLeft = this.game.add.button(this.game.world.centerX - 480, this.game.world.height - 100, 'btn_left', null, this);
    this.btnLeft.events.onInputOver.add(() => this.player.runLeft = true);
    this.btnLeft.events.onInputOut.add(() => this.player.runLeft = false);
    this.btnLeft.events.onInputDown.add(() => this.player.runLeft = true);
    this.btnLeft.events.onInputUp.add(() => this.player.runLeft = false);
    this.btnUp = this.game.add.button(this.game.world.centerX + 400, this.game.world.height - 100, 'btn_up', null, this);
    this.btnUp.events.onInputDown.add(this.player.jump, this.player);
  }

  update() {
    this.game.physics.arcade.collide(this.player, this.platforms, this.onPlayerPlatformCollision, null, this);
    this.game.physics.arcade.overlap(this.player, this.goat, this.onPlayerGoatOverlap, null, this);
    this.game.physics.arcade.collide(this.player, this.apple, this.onPlayerAppleCollision, null, this);
    this.game.physics.arcade.collide(this.arrows, this.platforms, this.onArrowPlatformCollision, null, this);

    if( this.player.y > 1300) {
      this.endGame();
    }

    this.platforms.forEach(platform => {
      if (platform.broken && !platform.dropping && !platform.body.touching.up) {
        platform.drop();
      }
    });
  }

  onPlayerPlatformCollision(player, platform) {
    if (!platform.broken && platform.body.touching.up) {
      platform.crack();
    }
  }

  onPlayerPlatformCollision(player, platform) {
    if (!platform.broken && platform.body.touching.up) {
      platform.crack();
    }
  }

  onPlayerGoatOverlap(player, goat) {
    goat.body.enable = false;
  }

  onPlayerAppleCollision(player, apple) {
    apple.kill();
    this.goat.body.enable = true;
  }

  onArrowPlatformCollision(arrow, platform) {
    if (platform.constructor === ArrowPlatform) {
      if (platform.index === this.arrows.indexOf(arrow)) {
        return;
      }
    }

    if (!platform.dropping) {
      platform.drop();
    }

    arrow.kill();
  }

  tweenApple(i, x, y) {
    let a = this.apples[i];
    a.position.set(x, y);
    a.visible = true;
    let tween = this.game.add.tween(a).to({ x: this.apple.x, y: this.apple.y }, 250);
    tween.onComplete.add(() => {
      this.apple.increase();
      a.visible = false;
    });
    tween.start();
  }

  showArrow(i, x, y) {
    let a = this.arrows[i];
    a.position.set(x, y);
    a.visible = true;
  }

  shootArrow(i) {
    let a = this.arrows[i];
    a.body.velocity.x = 1000;
  }

  endGame() {

    var slideOut = Phaser.Plugin.StateTransition.Out.SlideBottom;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideBottom;

    this.state.start('postgame', slideOut);
  }
}

export default Gameplay;
