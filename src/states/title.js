
class Title extends Phaser.State {
  create() {
    this.game.add.sprite(0, 0, 'bg_title');

    this.playButton = this.game.add.button(this.game.world.centerX + 360, this.game.world.centerY + 120, 'ui', this.startGameplay, this, 'btn_play_d.png', 'btn_play_u.png', 'btn_play_d.png');
    this.playButton.anchor.set(0.5, 0.5);

    this.howToButton = this.game.add.button(this.game.world.centerX + 360, this.game.world.centerY + 280, 'ui', this.startTutorial , this, 'btn_howto_d.png', 'btn_howto_u.png', 'btn_howto_d.png');
    this.howToButton.anchor.set(0.5, 0.5);
  }

  startGameplay() {

    this.clickButton();
    var slideOut = Phaser.Plugin.StateTransition.Out.SlideTop;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideTop;
    slideIn.duration = 1000;

    this.game.state.start('cutscene' , slideOut , false, "somethingasdf");
    
  }

  startTutorial() {

    this.clickButton();
    var slideOut = Phaser.Plugin.StateTransition.Out.SlideTop;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideTop;
    slideIn.duration = 1000;

    this.game.state.start('tutorial' , slideOut, slideIn );
    
  }

  clickButton() {

  }

}

export default Title;
