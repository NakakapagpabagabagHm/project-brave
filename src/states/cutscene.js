import CutsceneManager from '../objects/cutscene_manager';

class Cutscene extends Phaser.State {
  init(customParam){
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyDr671VmGkTqBhkuzWAT6TwmopthK9SYqQ",
      authDomain: "project-brave.firebaseapp.com",
      databaseURL: "https://project-brave.firebaseio.com",
      projectId: "project-brave",
      storageBucket: "project-brave.appspot.com",
      messagingSenderId: "1040551912715"
    };
    
    this.firebase = firebase.initializeApp(config);
    this.database = this.firebase.database();
  }
  
  create() {
    this.game.add.sprite(0, 0, 'bg_gameplay');

    //Local JSON Fetch
    //var cutsceneJSON = this.game.cache.getJSON('lorem_ipsum');

    //Online JSON Fetch
    //NOTE: Add loading screen here for downtime
    this.readCutsceneJSONOnline();
  }

  update() {

  }

  readCutsceneJSONOnline() {
    var cutsceneName = 'lorem_ipsum';
    this.database.ref('/cutscenes/' + cutsceneName).once('value').then(
      (snapshot) => {
        var cutsceneJSON = snapshot.val();
        var manager = new CutsceneManager(this.game, cutsceneJSON);
    });
  }
}

export default Cutscene;