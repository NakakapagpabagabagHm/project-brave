
class Postgame extends Phaser.State {
  create() {
    this.game.add.sprite(0, 0, 'bg_gameplay');

    this.replayButton = this.game.add.button(this.game.world.centerX, this.game.world.centerY, 'ui', this.restartGameplay, this, 'btn_play_d.png', 'btn_play_u.png', 'btn_play_d.png');
    this.replayButton.anchor.set(0.5, 0.5);

  }

  restartGameplay() {

    var slideOut = Phaser.Plugin.StateTransition.Out.SlideTop;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideTop;

    this.game.state.start('gameplay' , slideOut );
    
  }

}

export default Postgame;
