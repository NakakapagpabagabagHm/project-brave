
class Tutorial extends Phaser.State {
  create() {
    this.game.add.sprite(0, 0, 'bg_gameplay');

	this.leftButton = this.game.add.button( 0 , this.game.world.centerY, 'btn_left', this.leftButtonAction, this);
	this.leftButton.anchor.set(0.0, 0.5);

	this.rightButton = this.game.add.button(this.game.world.width , this.game.world.centerY, 'btn_right', this.rightButtonAction, this);
	this.rightButton.anchor.set(1.0, 0.5);

	this.playButton = this.game.add.button(this.game.world.centerX + 360, this.game.world.centerY + 120, 'ui', this.startGameplay, this, 'btn_play_d.png', 'btn_play_u.png', 'btn_play_d.png');
    this.playButton.anchor.set(0.5, 0.5);

    this.clickSFX = this.game.add.audio('sfx_button');
    
  }

  startGameplay() {

    this.clickButton();
    var slideOut = Phaser.Plugin.StateTransition.Out.SlideTop;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideTop;
    slideIn.duration = 1000;

    this.game.state.start('gameplay' , slideOut, slideIn );
  }

leftButtonAction () {
	this.clickButton();
	var slideOut = Phaser.Plugin.StateTransition.Out.SlideRight;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideRight;
    slideIn.duration = 1000;

	this.state.start('title' , slideOut , slideIn );
}

rightButtonAction () {
	this.clickButton();
	var slideOut = Phaser.Plugin.StateTransition.Out.SlideLeft;
    slideOut.duration = 1000;

    var slideIn = Phaser.Plugin.StateTransition.In.SlideLeft;
    slideIn.duration = 1000;

	this.state.start('tutorial' , slideOut , slideIn );
}

 clickButton() {
    this.clickSFX.play();

 }

}

export default Tutorial;
