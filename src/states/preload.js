class Preload extends Phaser.State {
  create() {
    let icon = this.game.add.image(this.game.world.centerX, this.game.world.centerY, 'icon_loading');
    icon.anchor.set(0.5, 0.5);

    let barBG = this.game.add.image(this.game.world.centerX, this.game.world.centerY + 180, 'bar_loading_bg');
    barBG.anchor.set(0.5, 0.5);

    let barFG = barBG.addChild(this.game.make.image(-barBG.width / 2, 0, 'bar_loading_fg'));
    barFG.anchor.set(0, 0.5);

    let barText = barBG.addChild(this.game.add.text(0, 4, 'LOADING'));
    barText.anchor.set(0.5, 0.5);
    barText.addColor('#ffffff', 0);

    this.game.load.setPreloadSprite(barFG, 0);
    this.game.load.onLoadComplete.add(this.loadComplete, this);

    this.game.load.image('bg_title', 'res/img/bg_title.png');
    this.game.load.image('bg_gameplay', 'res/img/bg_gameplay.png');
    this.load.atlas('steven', 'res/img/steven.png', 'res/img/steven.json');
    this.load.image('goat', 'res/img/goat.png');
    this.load.image('platform_normal', 'res/img/platform_normal.png');
    this.load.image('platform_large', 'res/img/platform_large.png');
    this.load.image('platform_gold', 'res/img/platform_gold.png');
    this.load.image('platform_arrow', 'res/img/platform_arrow.png');
    this.load.image('apple', 'res/img/apple.png');
    this.load.image('arrow', 'res/img/arrow.png');
    this.load.image('btn_right', 'res/img/btn_right.png');
    this.load.image('btn_left', 'res/img/btn_left.png');
    this.load.image('btn_up', 'res/img/btn_up.png');
    this.load.atlas('ui', 'res/img/ui.png', 'res/img/ui.json');
    this.load.json('level1', 'res/data/level1.json');

    this.game.load.audio('bgm_menu', 'res/audio/BGM_MainMenu.mp3');
    this.game.load.audio('sfx_button', 'res/audio/SFX_MenuButton.mp3');
    this.game.load.audio('sfx_flash', 'res/audio/SFX_Player_Camera.mp3');

    //Project Brave
    //Sample Assets
    this.load.image('text_box', 'res/img/sample_text_box.png');
    this.load.image('hero_sprite_cutscene', 'res/img/sample_unit_sprite.png');
    this.load.json('lorem_ipsum', 'res/data/lorem_ipsum.json');

    this.game.load.start();
  }

  loadComplete() {
    this.game.state.start('title');
  }
}

export default Preload;
