class CutsceneSprite extends Phaser.Sprite {
  constructor(game, manager, character, initial, spriteAnimations) {

    var initialXPos = 0;
    var initialYPos = 0;

    if (initial['x'] == "center") {
      initialXPos = game.world.centerX;
    } else {
      initialXPos = parseInt(initial['x']);
    }
    if (initial['y'] == "center") {
      initialYPos = game.world.centerY;
    } else {
      initialYPos = parseInt(initial['y']);
    }

    console.log("Sprite: " + character + " | Pos: ", initialXPos, initialYPos);

    super(game, initialXPos, initialYPos, 'hero_sprite_cutscene');
    game.add.existing(this);

    this.manager = manager;
    this.character = character;
    this.initial = initial;
    this.spriteAnimations = spriteAnimations;

    this.anchor.set(0.5, 0.5);

    this.dialogueIndex = 0;
    this.speed = 10;

    //Initialize Animation
    this.playAnimation();
  }

  update() {
    this.playingAnimation();
  }

  playAnimation() {
    //Map Tag to Animation function
    this.animating = true;

    var currentAnimation = this.spriteAnimations[this.dialogueIndex];
    this.animationTag = currentAnimation['tag'];

    console.log('animationTag: ' + this.animationTag);

    switch(this.animationTag) {
      case 0:
        //Slide
        this.endpoint = currentAnimation['endpoint'];
        break;
      case 1:
        //???
        // var startpoint = animation['startpoint'];
        // this.slideOut(startpoint);
        break;
      default:
        //Idle
        break;
    }
  }

  playingAnimation() {
    if (this.animating) {
      switch(this.animationTag) {
        case 0:
          //Slide
          this.slide();
          break;
        case 1:
          //???
          break;
        default:
          //Idle
          break;
      }
    }
  }

  nextAnimation(dialogueIndex) {
    this.dialogueIndex = dialogueIndex;
    this.playAnimation();
  }

  slide() {
    if(this.x < this.endpoint) {
      this.x = this.x + this.speed;
    }

    if(this.x > this.endpoint) {
      this.x = this.x - this.speed;
    }

    if( (this.x < this.endpoint && this.x > this.endpoint - this.speed) || (this.x > this.endpoint && this.x < this.endpoint + this.speed)) {
      this.x = this.endpoint;
      this.animating = false;
    }
  }
}

export default CutsceneSprite;
