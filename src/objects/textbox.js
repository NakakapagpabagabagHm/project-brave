class Textbox extends Phaser.Sprite {
  constructor(game, manager, content_text) {

    var XPOS = game.world.centerX;
    var YPOS = game.world.height - 50;

    super(game, XPOS, YPOS, 'text_box');
    game.add.existing(this);

    //Sprite
    //Width: 1279 | Height: 230
    this.XPOS = XPOS;
    this.YPOS = YPOS;
    
    this.WIDTH = 1279.0;
    this.HEIGHT = 230.0; 
    
    this.TEXT_X_SPACE = 50;
    this.TEXT_Y_SPACE = 40;
    
    this.anchor.set(0.5, 1);

    //Manager
    this.manager = manager;
    
    //Text
    this.TEXT_SPEED = 1; // 0 - Instant | 1 - Fast | 2 - Medium | 3 - Slow
    this.TEXT_SPEED_INTERVAL_FAST = 1;
    this.TEXT_SPEED_INTERVAL_MEDIUM = 5;
    this.TEXT_SPEED_INTERVAL_SLOW = 10;
    this.textTimer = 0;

    this.TEXT_SPEED_INTERVAL = this.TEXT_SPEED_INTERVAL_MEDIUM;
    if(this.TEXT_SPEED == 1){
      this.TEXT_SPEED_INTERVAL = this.TEXT_SPEED_INTERVAL_FAST;
    }  else if(this.TEXT_SPEED == 3) {
      this.TEXT_SPEED_INTERVAL = this.TEXT_SPEED_INTERVAL_SLOW;
    }

    this.MAX_TEXT_LENGTH = 230;
    this.WORDWRAP_WIDTH = 1150;

    this.style = { font: "32px Arial", fill: "#ff0044", align: 'left', wordWrap: true, wordWrapWidth: this.WORDWRAP_WIDTH };
    
    this.loadText(content_text);

    console.log('Content Length: ' + this.content.length);

    var text_xpos = ((game.world.width/2) - (this.WIDTH/2)) + this.TEXT_X_SPACE;
    var text_ypos = (game.world.height - (this.HEIGHT + 50)) + this.TEXT_Y_SPACE;

    this.text = game.add.text(text_xpos, text_ypos, "", this.style);
    this.resetIndex();
    this.splitTextContent();

    //Click Input
    this.inputEnabled = true;
    this.events.onInputDown.add(this.onClick, this);
  }

  resetIndex() {
    this.startingIndex = 0;
    this.currentIndex = 0;
    this.endingIndex = this.content.length;
  }

  update() {
    this.setText();
  }

  loadText(content_text) {
    //Set New Content
    this.content = content_text;

    //Reset Variables
    this.resetIndex();

    //Redisplay
    this.splitTextContent();
  }

  setText() {
    if(this.TEXT_SPEED != 0) {
      this.textTimer = this.textTimer + 1;
      if(this.textTimer >= this.TEXT_SPEED_INTERVAL) {
        
        var displayText = "";
        
        if(this.currentIndex < this.endingIndex) {
          this.currentIndex = this.currentIndex + 1;
        }

        displayText = this.content.slice(this.startingIndex, this.currentIndex);
        this.text.setText(displayText);

        this.textTimer = 0;
      }
    }
  }

  splitTextContent() {
    var displayText = "";

    var splitIndex = this.content.indexOf(' ', this.startingIndex + this.MAX_TEXT_LENGTH);
    console.log('Split Index: ' + splitIndex.toString());

    if(splitIndex < 0) {
      this.endingIndex = this.content.length;
      displayText = this.content.slice(this.startingIndex);
    } else {
      this.endingIndex = splitIndex;
      displayText = this.content.slice(this.startingIndex, splitIndex);
    }

    if(this.TEXT_SPEED == 0) this.text.setText(displayText);
  }

  onClick() {
    console.log('onClick');

    var splitIndex = this.content.indexOf(' ', this.startingIndex + this.MAX_TEXT_LENGTH);
    
    //If: Current dialogue not yet completed
    //Then: Display next of current dialogue
    if(splitIndex >= 0) {
      this.startingIndex = splitIndex + 1;
      this.currentIndex = this.startingIndex;
      this.splitTextContent();  
    }

    //If: Current Dialogue is completed
    //Then: Get next dialogue from manager
    if(splitIndex < 0) {
      this.manager.nextDialogue();
    }
  }
}

export default Textbox;
