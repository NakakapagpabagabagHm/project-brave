import utils from '../utils';

class Player extends Phaser.Sprite {
  constructor(game, x, y, key) {
    super(game, x, y, key);
    game.add.existing(this);

    this.speed = 4;
    this.runLeft = false;
    this.runRight = false;

    this.animations.add('idle', utils.createFrames(0, 5, 'idle', true), 12, true);
    this.animations.add('run', utils.createFrames(0, 6, 'run', false), 12, true);

    this.cursors = game.input.keyboard.createCursorKeys();
    this.cursors.up.onDown.add(this.jump, this);
  }

  jump() {
    if (this.body.touching.down) {
      this.animations.stop();
      this.body.velocity.y = -800;
      this.loadTexture('steven', 6);
    }
  }

  run(direction) {
    this.scale.x = direction;
    this.x += this.speed * direction;

    if (this.body.touching.down) {
      this.animations.play('run', true);
    }
  }

  idle() {
    this.animations.play('idle', true);
  }

  update() {
    if (this.cursors.left.isDown || this.runLeft) {
      this.run(-1);
    } else if (this.cursors.right.isDown || this.runRight) {
      this.run(1);
    } else {
      if (this.body.touching.down) {
        this.idle();
      } else {
        this.animations.stop();
        this.loadTexture('steven', 6);
      }
    }
  }
}

export default Player;
