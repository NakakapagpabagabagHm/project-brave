class Platform extends Phaser.Sprite {
  constructor(game, x, y, key) {
    super(game, x, y, key);
    game.add.existing(this);
    game.physics.arcade.enable(this);

    this.broken = false;
    this.dropping = false;
  }

  crack() {
    this.broken = true;
  }

  drop() {
    this.dropping = true;
    this.body.allowGravity = true;
  }
}

class LargePlatform extends Platform {
  constructor(game, x, y) {
    super(game, x, y, 'platform_large');
  }

  crack() {}
}

class GoldPlatform extends Platform {
  constructor(game, x, y) {
    super(game, x, y, 'platform_gold');
    this.game = game;
    this.index = void 0;
  }

  drop() {
    super.drop();
    this.game.state.getCurrentState().tweenApple(this.index, this.x, this.y);
  }
}

class ArrowPlatform extends Platform {
  constructor(game, x, y) {
    super(game, x, y, 'platform_arrow');
    this.game = game;
    this.index = void 0;
  }

  crack() {
    super.crack();
    this.game.state.getCurrentState().showArrow(this.index, this.x, this.y);
  }

  drop() {
    super.drop();
    this.game.state.getCurrentState().shootArrow(this.index);
  }
}

export {Platform, LargePlatform, GoldPlatform, ArrowPlatform};
