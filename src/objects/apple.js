class Apple extends Phaser.Sprite {
  constructor(game, x, y) {
    super(game, x, y, 'apple');
    game.add.existing(this);

    this.alpha = 0.5;

    this.foreground = this.addChild(game.make.sprite(0, 0, 'apple'));
    this.foreground.anchor.set(0.5, 0.5);
    this.foreground.alpha = 2;
  }

  setSize(size) {
    this.foreground.scale.set(size / 4, size / 4);
    this.size = size;
  }

  increase() {
    this.size++;
    this.foreground.scale.set(this.size / 4, this.size / 4);
    if (this.size >= 4) {
      this.body.enable = true;
    }
  }
}

export default Apple;
