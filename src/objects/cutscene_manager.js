import Textbox from './textbox';
import CutsceneSprite from './cutscene_sprite.js';

class CutsceneManager extends Phaser.Sprite {
  constructor(game, cutscene) {
    super(game, 0, 0);
    game.add.existing(this);

    this.game = game;

    //Complete Cutscene Object
    this.cutscene = cutscene;

    //Array of dialogue to be displayed
    this.dialogue = this.cutscene['dialogue'];

    //Array of initial sprite positions
    this.initial = this.cutscene['initial'];

    //Array of animations for sprites
    this.spriteAnimations = this.cutscene['animations'];

    //Initialize Variables
    this.dialogueIndex = 0;

    this.initializeSprites();
    this.initializeDialogue();
  }

  initializeSprites() {
    //Process Initial
    this.cutsceneSprites = {};
    for(var key in this.initial) {
      console.log('Char: ' + key);
      //Generate cutscene sprites
      var sprite = new CutsceneSprite(this.game, this, key, this.initial[key], this.spriteAnimations[key]);
      this.cutsceneSprites[key] = sprite;
    }
    
  }

  initializeDialogue() {
    //Generate cutscene dialogue
    this.textbox = new Textbox(this.game, this, this.dialogue[this.dialogueIndex]);
  }

  update() {
    
  }

  nextDialogue() {
    if(this.dialogueIndex < this.dialogue.length - 1){
      this.dialogueIndex = this.dialogueIndex + 1;
      this.textbox.loadText(this.dialogue[this.dialogueIndex]);
      this.nextSpriteAnimations();
    }
    
  }

  nextSpriteAnimations() {
    for(var key in this.initial) {
      this.cutsceneSprites[key].nextAnimation(this.dialogueIndex);
    }
  }
}

export default CutsceneManager;
