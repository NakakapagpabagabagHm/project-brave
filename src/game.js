import Boot from './states/boot';
import Preload from './states/preload';
import Title from './states/title';
import Tutorial from './states/tutorial';
import Gameplay from './states/gameplay';
import Cutscene from './states/cutscene';
import Postgame from './states/postgame';
import config from './config';
import transition from './plugins/phaser-state-transition.min';

class Game extends Phaser.Game {
  constructor() {
    super(config.gameWidth, config.gameHeight, Phaser.AUTO, 'game');

    this.state.add('boot', Boot);
    this.state.add('preload', Preload);
    this.state.add('title', Title);
    this.state.add('tutorial', Tutorial);
    this.state.add('gameplay', Gameplay);
    this.state.add('cutscene', Cutscene);
    this.state.add('postgame', Postgame);

    this.state.start('boot');
  }
}

new Game();
